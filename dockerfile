FROM python:3.7-alpine
WORKDIR /code
COPY . .
RUN apk update 
RUN apk add curl
RUN pip3 install -r requirements.txt
CMD ["python3", "randomgen.py"]service-2thestrider/service-2